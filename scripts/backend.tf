# Sets the "backend" used to store Terraform state.
# This is required to make continous delivery work.

terraform {
    backend "azurerm" {
        resource_group_name  = "flixtubetxuan"
        storage_account_name = "flixtubetxuan"
        container_name       = "flixtubetxuan"
        key                  = "terraform.tfstate"
    }
}